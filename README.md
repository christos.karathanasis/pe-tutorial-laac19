[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.ligo.org%2Fvirginia.demilio%2Fpe-tutorial-laac19/master)

# LAAC 2019 - PE tutorial

This is a LIGO Academic Advisory Committee (LAAC) tutorial for the LVC meeting
2019 on Parameter Estimation (PE) of gravitational waves signals. In this tutorial
we cover the basics of Bayesian Inference and learn how to use the lsc-soft packages
`bilby`, `bilby-pipe` and `PESummary`.

## Tutorial Plan
* Introduction to **Bayesian Inference** (Virginia d'Emilio)
* Parameter Estimation (PE) example with `bilby` (Moritz Huebner) - _notebook_
* Gravitational Wave specific PE example with `bilby` (Colm Talbot) - _notebook_
* Introduction to `bilby_pipe` (Greg Ashton)
* Visualising PE results with `pesummary` (Charlie Hoy)

## _Hands-on_ tutorials
This tutorial will be delivered through presentations and jupyter notebooks.
For running the _hands-on_ jupyter notebook tutorials, please refer to the
instructions in this repository INSTRUCTIONS.md.

