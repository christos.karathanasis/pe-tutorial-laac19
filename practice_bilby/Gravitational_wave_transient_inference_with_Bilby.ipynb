{
  "nbformat": 4,
  "nbformat_minor": 0,
  "metadata": {
    "colab": {
      "name": "Gravitational-wave transient inference with Bilby",
      "version": "0.3.2",
      "provenance": [],
      "collapsed_sections": []
    },
    "kernelspec": {
      "name": "python3",
      "display_name": "Python 3"
    },
    "accelerator": "GPU"
  },
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "Fvps0543kTqo",
        "colab_type": "text"
      },
      "source": [
        "# Astrophysical inference for gravitational-wave transients\n",
        "\n",
        "In this notebook we will go through some of the useful tools `Bilby` has for doing Bayesian inference on gravitational-wave transients.\n",
        "\n",
        "Out of necessity it will not be an exhaustive example of all the built in options and of course users are encouraged to augment `Bilby` themselves.\n",
        "\n",
        "There are far more [examples](https://git.ligo.org/lscsoft/bilby/tree/master/examples/gw_examples) than this in the `Bilby` git repository."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "gXe5czb3jXVo",
        "colab_type": "text"
      },
      "source": [
        "### Install packages\n",
        "\n",
        "Since this notebook was written for [colab](colab.research.google.com) we have to install the relevant packages in this case everything is available from `pip`.\n",
        "\n",
        "Copy and paste the following into a code cell below.\n",
        "\n",
        "```python\n",
        "!pip install bilby lalsuite gwpy\n",
        "```\n",
        "\n",
        "We install\n",
        "- `Bilby`: this is the library used for Bayesian inference, it also has a bunch of built in tools for gravitational-wave transients.\n",
        "- `LALSuite`: the LIGO Algorithms Library, `Bilby` (generally) uses `lalsimulation` to generate waveforms.\n",
        "- `gwpy`: a package for interfacing with gravitational-wave data, it is the most convenient way to access open or proprietary data."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "Ie_ycPGek-CN",
        "colab_type": "text"
      },
      "source": [
        "## Imports\n",
        "\n",
        "I'll put all (or at least most of) the imports here.\n",
        "Explanations of some of the specific elements will be where they're first used."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "UlY2Uc54jNnm",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "import matplotlib.pyplot as plt\n",
        "\n",
        "import bilby\n",
        "\n",
        "from bilby import run_sampler\n",
        "\n",
        "from bilby.core.prior import Constraint, Uniform\n",
        "\n",
        "from bilby.gw.conversion import (\n",
        "    convert_to_lal_binary_black_hole_parameters,\n",
        "    generate_all_bbh_parameters\n",
        ")\n",
        "from bilby.gw.detector.networks import InterferometerList\n",
        "from bilby.gw.detector.psd import PowerSpectralDensity\n",
        "from bilby.gw.likelihood import GravitationalWaveTransient\n",
        "from bilby.gw.prior import BBHPriorDict\n",
        "from bilby.gw.result import CBCResult\n",
        "from bilby.gw.source import lal_binary_black_hole\n",
        "from bilby.gw.utils import get_event_time\n",
        "from bilby.gw.waveform_generator import WaveformGenerator\n",
        "\n",
        "from gwpy.timeseries import TimeSeries"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "r4kYgF6JleY4",
        "colab_type": "text"
      },
      "source": [
        "## Accessing data\n",
        "\n",
        "In this example we're going to grab the data to analyse the end of the inspiral for GW170814.\n",
        "\n",
        "To make the analysis run in a reasonable time we're going to just look at the final ~3s of the inspiral.\n",
        "\n",
        "We ask for 128s of data after the signal in order to estimate the power spectral density.\n",
        "\n",
        "The methods below are adapted from [`bilby_pipe.data_generation`](https://git.ligo.org/lscsoft/bilby_pipe/blob/master/bilby_pipe/data_generation.py)."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "-7tFpOB0mY3w",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "interferometers = InterferometerList([\"H1\", \"L1\", \"V1\"])\n",
        "trigger_time = get_event_time(\"170814\")"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "B50pVUSJsbpB",
        "colab_type": "text"
      },
      "source": [
        "### First we need to specify various bits of information about how much data to get and how it should be processed.\n",
        "\n",
        "We fetch a 4s stretch of data and put the trigger time `3`s through the\n",
        "segment.\n",
        "\n",
        "We also specify the \"roll_off\", this tells use how quickly the [tukey window]() applied to the data before performing the fast fourier transform of the data should fall off.\n",
        "This is in units of seconds.\n",
        "A fall off of `0` is a rectangular window and leads to issues due to the \"lines\" (sharp features in the power spectrum).\n",
        "We should always be careful to not apply the window to part of the signal being analysed."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "02lrbi2isYkx",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "start_time = trigger_time - 3\n",
        "duration = 4\n",
        "end_time = start_time + duration\n",
        "roll_off = 0.2\n",
        "\n",
        "for interferometer in interferometers:\n",
        "    print(\n",
        "        \"Getting analysis segment data for {}\".format(interferometer.name)\n",
        "    )\n",
        "    analysis_data = TimeSeries.fetch_open_data(\n",
        "        interferometer.name, start_time, end_time\n",
        "    )\n",
        "    interferometer.strain_data.roll_off = roll_off\n",
        "    interferometer.strain_data.set_from_gwpy_timeseries(analysis_data)\n"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "MC1rlu8ZwZV8",
        "colab_type": "text"
      },
      "source": [
        "### Now we need to get the data to compute the power spectral density (PSD).\n",
        "\n",
        "We average over multiple segments.\n",
        "We take the median average."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "R800b5Nvtois",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "actual_psd_start_time = start_time + duration\n",
        "psd_duration = 128\n",
        "actual_psd_end_time = actual_psd_start_time + psd_duration\n",
        "psd_tukey_alpha = 2 * roll_off / duration\n",
        "overlap = duration / 2\n",
        "\n",
        "for interferometer in interferometers:\n",
        "    print(\"Getting psd segment data for {}\".format(interferometer.name))\n",
        "    psd_data = TimeSeries.fetch_open_data(\n",
        "        interferometer.name, actual_psd_start_time, actual_psd_end_time\n",
        "    )\n",
        "    psd = psd_data.psd(\n",
        "        fftlength=duration, overlap=overlap, window=(\"tukey\", psd_tukey_alpha),\n",
        "        method=\"median\"\n",
        "    )\n",
        "    interferometer.power_spectral_density = PowerSpectralDensity(\n",
        "        frequency_array=psd.frequencies.value, psd_array=psd.value\n",
        "    )\n"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "Ri3suek-weSv",
        "colab_type": "text"
      },
      "source": [
        "### Finally let's plot the data"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "Im0skix1vldL",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "for interferometer in interferometers:\n",
        "    plt.loglog(interferometer.frequency_array, abs(interferometer.frequency_domain_strain))\n",
        "    plt.loglog(interferometer.frequency_array, abs(interferometer.amplitude_spectral_density_array))\n",
        "    plt.xlim(interferometer.minimum_frequency, interferometer.maximum_frequency)\n",
        "    plt.show()\n",
        "    plt.close()"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "lJCFxg-Plm8K",
        "colab_type": "text"
      },
      "source": [
        "## Setting up our model"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "RsaAi7IewpxR",
        "colab_type": "text"
      },
      "source": [
        "### Now that we have data we need a model to construct out likelihood\n",
        "\n",
        "In this case our model will be a gravitational-wave template for a binary black hole merger.\n",
        "\n",
        "This is accessed using a `waveform_generator`.\n",
        "\n",
        "This is given:\n",
        "- the duration and sampling frequency of the data that we want to compare the model to.\n",
        "- a `frequency_domain_source_model`, this is a python function which reutrns the waveform template given the parameters. For details of how to define such a model see [here](https://git.ligo.org/lscsoft/bilby/blob/master/bilby/gw/source.py).\n",
        "- a `parameter_conversion` function, this enables the user to sample in parameters that aren't directly used by the source model, e.g., sampling in chirp mass and mass ratio instead of component masses.\n",
        "- `waveform_arguments`, this is a dictionary of arguments which are passed to the source model which should not be sampled, e.g., the waveform approximant/reference frequency.\n",
        "\n",
        "This source model calls down to `lalsimulation` and should be able to use any waveform model implemented in that pakage.\n",
        "\n",
        "Other options:\n",
        "- a `time_domain_source_model` can be passed as an alternative to the `frequency_domain_source_model`, this will then be FFT'd if used in the likelihood below."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "xlN1uDiFwpW9",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "waveform_arguments = dict(\n",
        "    waveform_approximant=\"IMRPhenomPv2\",\n",
        "    reference_frequency=20\n",
        ")\n",
        "\n",
        "waveform_generator = WaveformGenerator(\n",
        "    duration=interferometers.duration,\n",
        "    sampling_frequency=interferometers.sampling_frequency,\n",
        "    frequency_domain_source_model=lal_binary_black_hole,\n",
        "    parameter_conversion=convert_to_lal_binary_black_hole_parameters,\n",
        "    waveform_arguments=waveform_arguments\n",
        ")"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "3GU0ErXPlxqC",
        "colab_type": "text"
      },
      "source": [
        "## Prior choice\n",
        "\n",
        "`Bilby` has a built in `BBHPriorDict` class which has all the necessary parameters for sampling a binary black hole system.\n",
        "\n",
        "We'll use that as a base but then make a few adjustments.\n",
        "\n",
        "- we specify to use aligned spins rather than the full six-dimensional spins.\n",
        "- we need to add a prior for time as that is difficult to define a default prior for.\n",
        "- we remove the priors on the component masses and repace them with priors on the chirp mass and mass ratio as those are parameters which are easier to sample in."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "ycRlKeJrx_22",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "priors = BBHPriorDict(aligned_spin=True)\n",
        "priors[\"chirp_mass\"] = Uniform(\n",
        "    minimum=24, maximum=30,\n",
        "    latex_label=\"$\\\\mathcal{M}$\", unit=\"$M_{\\\\odot}$\", boundary=\"reflective\"\n",
        ")\n",
        "priors[\"mass_ratio\"] = Uniform(\n",
        "    minimum=0.4, maximum=1, latex_label=\"$q$\", boundary=\"reflective\"\n",
        ")\n",
        "del priors[\"mass_1\"], priors[\"mass_2\"]\n",
        "\n",
        "priors[\"geocent_time\"] = Uniform(\n",
        "    minimum=trigger_time - 0.1, maximum=trigger_time + 0.1,\n",
        "    latex_label=\"$t_c$\", unit=\"$s$\", boundary=\"reflective\"\n",
        ")"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "gPAVxPdvvIjK",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "for key in priors:\n",
        "    print(f\"{key}: {priors[key]}\")"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "SaZN6wTtvJTl",
        "colab_type": "text"
      },
      "source": [
        "Let's take a look at what priors we have.\n",
        "\n",
        "These are mostly uniform with just a few exceptions.\n",
        "\n",
        "- Since we're only considering aligned spins we are using the `bilby.gw.prior.AlignedSpin` class.\n",
        "This takes a prior on spin magnitude $a$ and _cosine_ spin tilt $z$ and calculates the implied prior on the aligned component of the spin.\n",
        "- For luminosity distance we use the `bilby.gw.prior.UniformSourceFrame` prior. This ensures that our prior belief on distance is that events happen uniformly throughout the universe. The `cosmology` argument can either be an astropy `Cosmology` object or the name of a known cosmology, e.g., `WMAP9`.\n",
        "- `dec` and `theta_jn` have `Cosine` and `Sine` priors respectively, these are saying that the prior distribution is proportional to the appropriate trignometric function.\n",
        "These are chose so that the prior is uniform over the sphere."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "NYmn_DbXjAwE",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "prior_samples = priors.sample(10000)\n",
        "\n",
        "plt.hist(prior_samples[\"chi_1\"], bins=100)\n",
        "plt.show()\n",
        "plt.close()\n",
        "\n",
        "plt.hist(prior_samples[\"luminosity_distance\"], bins=100)\n",
        "plt.show()\n",
        "plt.close()"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "NE0DKfntlpc5",
        "colab_type": "text"
      },
      "source": [
        "## Let's make a likelihood!\n",
        "\n",
        "The standard likelihood used in gravitational-wave transient inference is the [Whittle Likelihood](https://en.wikipedia.org/wiki/Whittle_likelihood)\n",
        "$$\\mathcal{L}(d | \\theta) = \\prod_{i} \\prod_{j} \\frac{1}{2 \\pi S_{i,j} } \\exp\\left( -\\frac{|\\tilde{d}_{i,j} - \\tilde{h}_{i,j}(\\theta)|^2}{2S_{i,j} } \\right).$$\n",
        "The product here is over detectors $i$ and frequency bins $j$.\n",
        "$\\tilde{d}$ is the frequency-domain strain data, $\\tilde{h}$ is the template waveform, and $S$ is the noise power spectral density.\n",
        "\n",
        "In practice we are only interested in the (logarithm of) the terms which depend on the binary parameters.\n",
        "In this case we define the \"log likelihood ratio\" as follows\n",
        "$$\\ln \\mathcal{L} = \\sum_{j} \\sum_{j} - \\frac{1}{2} |\\tilde{h}_{i,j}|^2 + \\mathbb{R}\\left( \\tilde{d}_{i,j}^{*} \\tilde{h}_{i,j} \\right).$$\n",
        "\n",
        "_Note_: There are exceptions, e.g., when the power spectral density is modeled.\n",
        "\n",
        "There are some parameters which alter the template (and by extension the likelihood) in simple ways. The `GravitationalWaveTransient` likelihood in `Bilby` has implementation of marginalisation over the distance, time, and phase.\n",
        "\n",
        "The likelihood takes the following arguments\n",
        "- `interferometers`: this is the data for our likelihood\n",
        "- `waveform_generator`: this is our model for the signal\n",
        "- `priors`: if we use any of the marginalisations we need to provide the prior. The priors for the marginalised parameters are modified in place to fiducial values\n",
        "- `distance_marginalization`: whether to marginalize the likelihood over distance. I generally recommend using this as it removes the distance/inclination degeneracy and the posterior can be generated in post-processing. I've disabled it in this example as it takes a few minutes to set up the required lookup table.\n",
        "- `phase_marginalization`: whether to marginalize the likelihood over phase. Marginalising over phase analytically removes the degeneracy with polarisation. Note that this is only valid when there are no higher-order modes in the waveform.\n",
        "- `time_marginalization`: whether to marginalize the likelihood over time. This uses a FFT to marginalise over time.\n",
        "- `jitter_time`: when using the `time_marginalization` option the likelihood is evaluated on a fixed grid of times. To avoid coarse-graining issues on this grid we have the ablililty to sample in the position of the grid.\n",
        "\n",
        "_Note_: If you're using `bilby<0.5.6` there is a message `Time jittering requested with non-time-marginalised likelihood, ignoring.`. Ignore this."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "aOb4fmu7xtxm",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "likelihood = GravitationalWaveTransient(\n",
        "    interferometers=interferometers, waveform_generator=waveform_generator,\n",
        "    priors=priors, time_marginalization=False, distance_marginalization=False,\n",
        "    phase_marginalization=True\n",
        ")"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "n6qOe8Vbl_g9",
        "colab_type": "text"
      },
      "source": [
        "## It's samplin' time\n",
        "\n",
        "To make this run in just a few minutes the sampler setting are woefully inadequate to have faith in the results.\n",
        "\n",
        "The do a better run increase `nlive` and `walks`, for the review we are using nlive $= O(1000)$ and walks $= O(100)$.\n",
        "For details on the specific sampler and others, see the documentation either in [`Bilby`](https://docs.ligo.org/lscsoft/bilby/samplers.html) or the individual samplers, e.g., [`dynesty`](https://dynesty.readthedocs.io/en/latest/api.html).\n",
        "\n",
        "We pass a `conversion_function` which is applied after the sampling has finished. In this case we tell it to `generate_all_bbh_parameters`, this generates all the parameters that `Bilby` knows how to produce for binary black hole systems. This includes constructing the posterior for the marginalised parameters."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "LdnFZAQEz77b",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "result = run_sampler(\n",
        "    likelihood=likelihood, priors=priors, save=False,\n",
        "    nlive=50, walks=25,\n",
        "    conversion_function=generate_all_bbh_parameters,\n",
        "    result_class=CBCResult)"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "sC7I-6qomJtN",
        "colab_type": "text"
      },
      "source": [
        "## and finally some plotting.\n",
        "\n",
        "First we'll make plots of the inferred waveform. Hopefully this will line up with a feature in the whitened time-domain strain."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "R6wxfsXA0cBt",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "for interferometer in interferometers:\n",
        "    fig = result.plot_interferometer_waveform_posterior(\n",
        "        interferometer=interferometer\n",
        "    )\n",
        "    plt.show()\n",
        "    plt.close()"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "SfVw1zb0x9Lz",
        "colab_type": "text"
      },
      "source": [
        "Now let's make a couple of corner plots on same parameters.\n",
        "\n",
        "In this first one we plot the source-frame masses and effective aligned spin, then we'll look at the distance and right ascension/declination."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "f1NUyDG77szh",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "result.plot_corner(parameters=[\"mass_1_source\", \"mass_2_source\", \"chi_eff\"])"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "NYA463zT-OeN",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "result.plot_corner(parameters=[\"luminosity_distance\", \"ra\", \"dec\"])"
      ],
      "execution_count": 0,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "nQtY18Vgl1m0",
        "colab_type": "text"
      },
      "source": [
        "## Bonus cells\n",
        "\n",
        "Here's a small set of examples showing other things which are possible."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "PbSMBFnbyR6H",
        "colab_type": "text"
      },
      "source": [
        "## Calibration\n",
        "\n",
        "It is possible to include a model for the detector calibration.\n",
        "This is an attribute of the `Interferometer` object.\n",
        "Currently the calibration model is limited to be a cubic spline as implemented in `LALInference` however the user can provide their own model, or contribute one to `Bilby`!\n",
        "\n",
        "```python\n",
        "from bilby.gw.detector.calibration import CubicSpline\n",
        "interferometer.calibration_model = CubicSpline(\n",
        "    prefix=interferometer.name,\n",
        "    minimum_frequency=interferometer.minimum_frequency,\n",
        "    maximum_frequency=interferometer.maximum_frequency,\n",
        "    n_points=9\n",
        ")\n",
        "```\n",
        "\n",
        "There are also tools for creating the required prior for the spline calibration.\n",
        "\n",
        "```python\n",
        "from bilby.gw.prior import CalibrationPriorDict\n",
        "priors.update(CalibrationPriorDict.constant_uncertainty_spline(\n",
        "    amplitude_sigma=0.1,\n",
        "    phase_sigma0.03,\n",
        "    minimum_frequency=interferometer.minimum_frequency,\n",
        "    maximum_frequency=interferoemter.maximum_frequency,\n",
        "    n_nodes=9,\n",
        "    label=interferometer.name):\n",
        ")\n",
        "```"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "jvCfDgXf0Nf5",
        "colab_type": "text"
      },
      "source": [
        "## Generating fake data\n",
        "\n",
        "A standard use-case for `Bilby` is analysing signals injected into fake data.\n",
        "The standard example for this is [here](https://git.ligo.org/lscsoft/bilby/blob/master/examples/gw_examples/injection_examples/fast_tutorial.py)\n",
        "\n",
        "Creating the data and injecting the signal in this case is by doing the following.\n",
        "\n",
        "```python\n",
        "injection_parameters = dict(\n",
        "    mass_1=36., mass_2=29., a_1=0.4, a_2=0.3, tilt_1=0.5, tilt_2=1.0,\n",
        "    phi_12=1.7, phi_jl=0.3, luminosity_distance=2000., theta_jn=0.4, psi=2.659,\n",
        "    phase=1.3, geocent_time=1126259642.413, ra=1.375, dec=-1.2108)\n",
        "\n",
        "ifos = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1', 'K1'])\n",
        "ifos.set_strain_data_from_power_spectral_densities(\n",
        "    sampling_frequency=sampling_frequency, duration=duration,\n",
        "    start_time=injection_parameters['geocent_time'] - 3)\n",
        "ifos.inject_signal(waveform_generator=waveform_generator,\n",
        "                   parameters=injection_parameters)\n",
        "```"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "NpihSLvrEQ92",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        ""
      ],
      "execution_count": 0,
      "outputs": []
    }
  ]
}